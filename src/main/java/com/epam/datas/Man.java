package com.epam.datas;

import java.io.Serializable;

public class Man implements Serializable {
    private String name;
    private int age;
    public static int count = 25;
    private transient String street;
    public Man(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }
}
