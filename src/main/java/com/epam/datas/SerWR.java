package com.epam.datas;

import java.io.*;

public class SerWR {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream("man.out"));
        Man manWrite = new Man("Max", 20);
        objectOutputStream.writeObject(manWrite);
        objectOutputStream.close();
        manWrite.setAge(21);
        System.out.println(manWrite.getAge() + " " + manWrite.getName() + " STATIC = " + Man.count);
        Man.count = 50;
        ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream("man.out"));
        Man manRead = (Man) objectInputStream.readObject();
        System.out.println(manRead.getAge() + " " + manRead.getName() + " STATIC = " + Man.count);
        objectInputStream.close();
    }
}
