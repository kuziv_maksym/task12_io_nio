package com.epam.dir;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Directory {
    public static Logger logger = LogManager.getLogger(Directory.class);
    private void shows() throws Exception{
        File file = new File("F:\\Epam");
        if (file.exists()){
            printDir(file, "");
        } else {
            System.out.println("Dir not exists");
        }
    }

    private void printDir(File file, String name) {
        System.out.println(name + " Dir" + file.getName());
        name = name + " ";
        File[] fileName = file.listFiles();
        for (File f: fileName) {
            if (f.isDirectory()) {
                printDir(f, name);
            }else {
                System.out.println(name + " file" + file.getName());
            }
        }
    }

    public static void main(String[] args) {
        Directory directory = new Directory();
        try {
            directory.shows();
        } catch (Exception e) {
            logger.trace("File except");
            e.printStackTrace();
        }
    }
}
