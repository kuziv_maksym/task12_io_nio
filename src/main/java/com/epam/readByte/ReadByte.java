package com.epam.readByte;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class ReadByte {
    public static Logger logger = LogManager.getLogger(ReadByte.class);
    static int count = 0;
    public static void main(String[] args) {
        try {
            InputStream os = new FileInputStream("newfile.txt");
            int data = os.read();
            while (data != -1) {
                count++;
                data = os.read();
            }
            System.out.println(count);
            os.close();
        } catch (FileNotFoundException e) {
            logger.trace("File not found");
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Exception read file");
            e.printStackTrace();
        }

    }
}
