package com.epam.readFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class ReadWriteFile {
    public static Logger logger = LogManager.getLogger(ReadWriteFile.class);
    static final int[] count = {1, 2, 3, 4, 5};
    static final int[] price = {10, 20, 30, 40, 50};
    static final String[] name = {"Один", "Two", "Three", "Four", "Five"};
    public static void main(String[] args) {
        int count1;
        int price1;
        String name1;
        try {
                OutputStream outputStream = new FileOutputStream("newfile.txt");
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
                DataOutputStream dataOutputStream = new DataOutputStream(bufferedOutputStream);
            for (int i = 0; i < count.length; i++) {
                dataOutputStream.writeInt(count[i]);
                dataOutputStream.writeInt(price[i]);
                dataOutputStream.writeUTF(name[i]);
            }
            dataOutputStream.close();
            bufferedOutputStream.close();
            outputStream.close();
        } catch (FileNotFoundException e) {
            logger.trace("File not found");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("FileError");
        }
        try (
                DataInputStream dis = new DataInputStream(
                        new BufferedInputStream(
                                new FileInputStream("newfile.txt")
                        )
                );
        ) {
            while (dis.available() > 0) {
                count1 = dis.readInt();
                price1 = dis.readInt();
                name1 = dis.readUTF();
                System.out.println(count1 + " " + price1 + " " + name1);
            }
        } catch (FileNotFoundException e) {
            logger.trace("File not found");
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("FileError");
            e.printStackTrace();
        }
    }
}
